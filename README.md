Comment construire le PDF ?
===========================

La voie la plus directe pour l'utilisateur passe par le gestionnaire de paquets
[Nix](https://nixos.org/nix/). Une fois installé, il vous suffit juste de lancer

    $ nix-shell --pure

pour lancer un environnement de compilation continue.

Le PDF est également disponible [directement](https://framagit.org/mpo/doctorat-soutenance/blob/master/presentation.pdf).
