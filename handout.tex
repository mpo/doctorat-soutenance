\documentclass{article}

\newcommand{\action}[1]{\emph{(#1)}}

\begin{document}


\input{title}
Mesdames, Messieurs, bienvenue à ma soutenance de thèse de doctorat en
informatique.
Je souhaiterai commencer cette présentation en remerciant les membres de mon
jury, ici présents, d'avoir accepté notre invitation à participer à cette
soutenance.

Entrons, si vous le voulez bien, dans le vif du sujet.

\input{synbiotic.presentation}
Comme vous le savez peut-être, ma thèse a été en partie financée sur le
projet ANR blanc SYNBIOTIC, et mon sujet de thèse est directement issue des
problématiques abordées par ce projet. SYNBIOTIC s'est étalé sur 3 ans, de
2011 à 2014, et a mobilisé une quinzaine de chercheurs, principalement en
informatique. Comme l'annonce la présentation officielle du projet:
\begin{citation}
  La motivation finale est de permettre l'exploitation des propriétés
  collectives d'une population bactérienne pour créer des biosystèmes
  artificiels répondant à divers besoins dans le domaine de la santé, des
  nanotechnologies, de l'énergie et de la chimie.
\end{citation}
Il s'agit ici de suivre le principe même de la biologie synthétique: monter
en abstraction et oublier les détails de fonctionnement, car l'objectif est
de \emph{spécifier} un comportement de la population. Par exemple, omettre
le détail du fonctionnement d'un réseau de régulation génétique au profit de
l'ingénierie directe des formes.

\action{montrer la flèche vers le haut sur le schéma}

L'approche empruntée est issue d'un domaine ayant fait ses preuves en ce qui
concerne la gestion de la complexité: l'informatique. L'idée est d'utiliser
le principe de la \emph{compilation}, qui permet de transformer un programme,
décrivant un comportement abstrait, vers une série de chiffres binaires exécutés
par le processeur.

Nous utilisons ce procédé pour constituer le chemin du retour, de la
spécification d'une forme à l'échelle de la population vers le réseau de
régulation génétique de chaque individu. Le projet décrit ainsi une tour de
langages (informatique) permettant de compiler successivement la spécification
des formes vers un langage de programmation spatiale (L1), de L1 vers un langage
de programmation orienté entitée (L2) et enfin de L2 vers un langage orienté
réseau de régulation génétique (L3).

Au cours de mon travail de thèse, je suis intervenu à différents étages de cette
tour: dans la deuxième partie de cette présentation nous parlerons de l'activité
dans MGS, le langage L1 et dans la troisième nous parlerons de OTB un simulateur
dont le langage d'entrée est L2.

\input{problem.statement}
Comme je viens de vous le décrire, il suffit de trouver un compilateur pour
passer d'un étage à l'étage inférieur de cette tour. Malheureusement, ça n'est
pas aussi simple.

Je souhaiterai attirer votre attention sur deux points essentiels qui me
permettront de poser la problématique. Le premier porte sur la sémantique.

(en pointant le schéma compilation)
I. En informatique.

Soit une fonction f, dans un certain langage de programmation bien connu,
disons C. Après compilation, nous obtenons cette suite d'instructions en
assembleur intel x86. Ces instructions peuvent être exécutées par le processeur
de votre ordinateur et le comportement que vous pouvez observer correspond bien,
par construction, à la fonction f.

Imaginons que vous n'aviez pas connaissance de cette fonction, et que vous
disposiez uniquement des instructions en assembleur. Avec beaucoup de travail,
vous pourriez deviner quel était la fonction f. Cette activité a d'ailleurs un
nom, c'est le reverse-engineering.

C'est mon premier point: la fonction f \emph{n'existe pas} en tant qu'objet
de première classe dans les instructions en assembleur. Cette fonction
disparaît à la compilation. Le vocabulaire des deux niveaux est distinct, mais
néammoins suffisamment proche pour pouvoir les lier (c'est bien là le rôle du
compilateur).

II. En biologie.

La difficulté est d'un cran supérieur: cette fonction f est issue d'un
support partiellement connu, pas des effets d'émergence. Les interactions au
niveau local sont complexes (1+1=3) et dûrement prédictibles. Notre but est
d'ingénierer l'émergence (WP1).

Problématique : le niveau local et le niveau global décrivent deux mondes, deux
univers différents que nous chercherons à lier.

Comment parler en même temps du niveau local et du niveau global dans une seule
et même description ? Nous apportons des pistes de réflexion dans la première
partie de cette présentation.

\input{plan}
C'est le plan

\mkPart{Multi-niveau}
\input{multimodel}
Nous l'avons vu en introduction, notre problématique est de décrire le lien
qu'il existe entre deux mondes: le niveau global et le niveau local. Ce sont
potentiellement deux modèles décrivant le \emph{même système} de deux points
de vues différents.

Nous nous sommes d'abord interessés à ce que l'on trouve dans la littérature en
ce qui concerne la modélisation multimodèle. Voici trois exemples emblématiques
de trois manières d'envisager la question:

(figure)

1) Couplage de modèles
Nous prenons l'exemple d'un multimodèle d'une cellule entière (image de la
publication). 28 modèles tirés d'études antérieures sont rassemblés et mettent à
jour un ensemble de 16 variables propres à la cellule. Ces modèles sont exécutés
en isolation et en parallèle. Une partie du travail de cette équipe de recherche
a été d'arbitrer la mise à jour des valeurs de chacune des variables de la
cellule. En effet ces variables sont lues et écrites par chacun des modèles.
Elles constituent de plus le lien entre chacun de ces modèles.

Cette construction est adhoc car elle dépend explicitement des spécificités de
chacun des modèles. Elle nécessite d'ajouter des programmes pour effectuer
la traduction en entrée et en sortie de chaque modèle importé. C'est un
fonctionnement en boîte noire où on ne connaît que les entrées et sorties des
modèles.

2) Transformation de modèles
Nous prenons l'exemple de l'ingénierie des modèles en informatique, dont le but
est de travailler \emph{directement} sur les modèles, et de les faire évoluer au
moyen de transformations de modèles. Par exemple, il est possible de spécifier
l'ajout d'une fonctionnalité à un programme en conservant certaines propriétés,
directement au niveau du modèle. Un exemple emblématique de l'ingénierie des
modèles est le langage UML (pour Unified Model Language). Dans ce langage de
spécification graphique, un modèle est constitué d'un ensemble diagrammes
décrivant son fonctionnement. Cette description opère à un niveau plus abstrait
que celui de l'implémentation.

3) Complexification
Nous prenons cette fois sur la méthodologie MENS (Memory Evolutive Neural
Systems) dont une des applications proposée porte sur la modélisation des
interactions neuronales.



\input{formalism.model.system}
\input{experimental.model}
\input{predator.prey}
\input{model.category}
%\input{representing.space}
%\input{representing.time}
%\input{representing.space.time.interaction}
\mkPart{Activité spatiale}
\input{mgs.topological.collections}
\input{mgs.topological.transformations}
\input{simple.ff.example}
\input{activity.in.mgs}
\input{where.is.activity}
\input{wave.algorithm}
\input{example.forest.fire}
\input{example.dla}
\mkPart{OTB}
\input{otb.general}
\input{otb.architecture}
\input{sbgp}
\input{ecoli.model}
\input{chemical.model}
\input{decision.model}
\input{otb.sectorisation}
\input{otb.stable.pop}
\input{otb.ppm}
\input{closing}
\end{document}
