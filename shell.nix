#To run this, you need nix (https://ariya.io/2016/05/nix-as-os-x-package-manager)
#and the package nix-shell (just run `nix-env -iA nixos.nix-shell`)
#Then, in the directory of this file, just call `nix-shell`

#This will create a custom shell (with only the needed dependencies) for
#building the presentation
with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "thesis-presentation";
  shellHook = ''
    latexmk \
      -pvc \
      -view=none \
      -silent \
      -time \
      -interaction=nonstopmode \
      -xelatex main.tex
    latexmk -c
    rm *.{snm,nav,vrb}
  '';
  buildInputs = [
    ( texlive.combine {
        inherit (texlive)
        scheme-basic
        collection-langfrench
        #algorithm2e
        #biblatex
        #caption
        #enumitem
        #logreq
        #minitoc
        #multirow
        #pgf
        #pgfplots
        #placeins
        #relsize
        #setspace
        #standalone
        #xcolor
        #xkeyval
        #xstring
        beamer
        colortbl
        euenc
        extsizes
        filehook
        jknapltx
        latexmk
        listings
        metafont
        ms
        pdfpages
        polyglossia
        realscripts
        rsfs
        siunitx
        tools
        ucharcat
        unicode-math
        xetex
        xetex-def
        xltxtra
        zapfding
      ;} )
      #biber
  ];
}
#thesis = buildEnv {
#  name = "memoire-these";
#  buildInputs = [
#  ];
#  shellHook = ''
#    alias make=$(which buildthesis)
#    ln -s $(which buildthesis) buildme
#    echo "Juste type 'make' or './buildme' to build the thesis"
#  '';
#};
