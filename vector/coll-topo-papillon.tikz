    \begin{tikzpicture}
      [ scale=0.4,
        cellz/.style=
        {circle,
         inner sep=0.5mm,
         fill=black}]
      % Le papillon !

      % Notable positions
      \coordinate (head)    at  (0, 0);
      \coordinate (r0)      at  (1, 2);
      \coordinate (r1)      at  (5, 3);
      \coordinate (r2)      at  (7, 2);
      \coordinate (r3)      at  (4,-3);
      \coordinate (r4)      at  (5,-7);
      \coordinate (r5)      at  (1,-8);
      \coordinate (l0)      at  (-1, 2);
      \coordinate (l1)      at  (-5, 3);
      \coordinate (l2)      at  (-7, 2);
      \coordinate (l3)      at  (-4,-3);
      \coordinate (l4)      at  (-5,-7);
      \coordinate (l5)      at  (-1,-8);
      \coordinate (m1)      at  (0,-8,1);

      % 3D
      \uncover<3-5,6>{
        %\fill           (head.center) -- (r5.center) -- (l5.center) -- (head.center);
        \fill[gray]     (head.center) -- (m1.center) -- (r5.center) -- (head.center);
        \fill[gray!20]  (head.center) -- (m1.center) -- (l5.center) -- (head.center);
      }
      \uncover<7->{
        \fill[green]    (head.center) -- (m1.center) -- (r5.center) -- (head.center);
        \fill[green!30] (head.center) -- (m1.center) -- (l5.center) -- (head.center);
      }

      % 2D
      \uncover<3,5>{
        \shade[right color=gray!20,left color=gray]
        (head.center) -- (r1.center) -- (r2.center) -- (r3.center)
        -- (r4.center) -- (r5.center) -- (head.center);
        \shade[left color=gray!20,right color=gray]
        (head.center) -- (l1.center) -- (l2.center) -- (l3.center)
        -- (l4.center) -- (l5.center) -- (head.center);
      }
      \uncover<6>{
        \shade[right color=gray!20,left color=gray]
        (head.center) -- (r1.center) -- (r2.center) -- (r3.center)
        -- (r4.center) -- (r5.center) -- (head.center);
        \shade[left color=alert!20,right color=alert]
        (head.center) -- (l1.center) -- (l2.center) -- (l3.center)
        -- (l4.center) -- (l5.center) -- (head.center);
      }
      \uncover<7->{
        \shade[right color=blue!20,left color=blue]
        (head.center) -- (r1.center) -- (r2.center) -- (r3.center)
        -- (r4.center) -- (r5.center) -- (head.center);
        \shade[left color=orange!20,right color=orange]
        (head.center) -- (l1.center) -- (l2.center) -- (l3.center)
        -- (l4.center) -- (l5.center) -- (head.center);
      }

      % 1D
      \uncover<2,5,6>{
        \draw[thick,black] (head) -- (r1) -- (r2) -- (r3) -- (r4) -- (r5) -- (head);
        \draw[thick,black] (head) -- (l1) -- (l2) -- (l3) -- (l4) -- (l5) -- (head);
        \draw[thick,black] (l5) -- (m1) -- (r5);
        \draw[thick,black] (head) -- (m1);
        \draw[black,dashed] (l5) -- (r5);
        \draw[thick,black] (head) to [out=90,in=-150] (r0);
        \draw[thick,black] (head) to [out=90,in= -30] (l0);
      }
      \uncover<7->{
        \draw[thick,red] (head) -- (r1) -- (r2) -- (r3) -- (r4) -- (r5) -- (head);
        \draw[thick,pink] (head) -- (l1) -- (l2) -- (l3) -- (l4) -- (l5) -- (head);
        \draw[thick,red] (l5) -- (m1) -- (r5);
        \draw[thick,black] (head) -- (m1);
        \draw[black,dashed] (l5) -- (r5);
        \draw[thick,gray] (head) to [out=90,in=-150] (r0);
        \draw[very thick,black] (head) to [out=90,in= -30] (l0);
      }

      % 0D
      \uncover<1,5,6>{
        \node[cellz] at (head) {};
        \node[cellz] at (r0)   {};
        \node[cellz] at (r1)   {};
        \node[cellz] at (r2)   {};
        \node[cellz] at (r3)   {};
        \node[cellz] at (r4)   {};
        \node[cellz] at (r5)   {};
        \node[cellz] at (l0)   {};
        \node[cellz] at (l1)   {};
        \node[cellz] at (l2)   {};
        \node[cellz] at (l3)   {};
        \node[cellz] at (l4)   {};
        \node[cellz] at (l5)   {};
        \node[cellz] at (m1)   {};
      }
      \uncover<6>{
        %\draw[overlay] (e.east) -- (l0);
        %\draw[overlay,-to,shorten >=5pt,thin] (e.east) .. controls +(1,0) .. (barycentric cs:l1=1,l2=1);
        %\draw[overlay,-to,shorten >=5pt,thin] (e.east) .. controls +(1,0) .. (barycentric cs:l2=1,l3=1);
        %\draw[overlay,-to,shorten >=5pt,thin] (e.east) .. controls +(1,0) .. (barycentric cs:l3=1,l4=1);
        %\draw[overlay,-to,shorten >=5pt,thin] (e.east) .. controls +(1,0) .. (barycentric cs:l4=1,l5=1);
        %\draw[overlay,-to,shorten >=5pt,thin] (e.east) .. controls +(1,0) .. (barycentric cs:l5=1,head=1);
        %\draw[overlay,-to,shorten >=5pt,thin] (e.east) .. controls +(1,0) .. (barycentric cs:head=1,l1=1);
        \draw[overlay,-to,shorten >=2pt,thin,out=0,in=-90] (e.east) to (barycentric cs:l1=1,l2=1);
        \draw[overlay,-to,shorten >=2pt,thin,out=0,in=-110] (e.east) to (barycentric cs:l2=1,l3=1);
        \draw[overlay,-to,shorten >=2pt,thin,out=0,in=180] (e.east) to (barycentric cs:l3=1,l4=1);
        \draw[overlay,-to,shorten >=2pt,thin,out=0,in=-135] (e.east) to (barycentric cs:l4=1,l5=1);
        \draw[overlay,-to,shorten >=2pt,thin,out=0,in=180] (e.east) to (barycentric cs:l5=1,head=1);
        \draw[overlay,-to,shorten >=2pt,thin,out=0,in=-90] (e.east) to (barycentric cs:head=1,l1=1);
      }
      \uncover<7->{
        \node[cellz,red] at (head) {};
        \node[cellz,green] at (r0)   {};
        \node[cellz,red] at (r1)   {};
        \node[cellz,pink] at (r2)   {};
        \node[cellz,red] at (r3)   {};
        \node[cellz,gray] at (r4)   {};
        \node[cellz,red!50] at (r5)   {};
        \node[cellz,blue!40] at (l0)   {};
        \node[cellz,red!10] at (l1)   {};
        \node[cellz,green] at (l2)   {};
        \node[cellz,pink] at (l3)   {};
        \node[cellz,black] at (l4)   {};
        \node[cellz,blue!90] at (l5)   {};
        \node[cellz,red] at (m1)   {};
      }
    \end{tikzpicture}
